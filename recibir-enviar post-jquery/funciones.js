$("#btnClick").click(function(){
    
    // Enviar datos al request de tipo POST
    $.post(
    
    // La dirección de la consulta
    'acciones.php', 
    
    // Datos a enviar
    {
        nombre: "Jesus Lopez",
        ciudad: "Lima",
        edad: 34,
        telefono: 1234567890   
    }, 

    // Callback ... que hacer con la respuesta de la consulta (Opcional)
    function(datos, estado){
        // alert("Informacion: " + datos + "\nEstados: " + estado);
        $("#response").html(datos);
    });

});