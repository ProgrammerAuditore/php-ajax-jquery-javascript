const $btnEnviar = document.querySelector("#btn-enviar");
$btnEnviar.onclick = function(){ fncObtenerArray(); };

function fncObtenerArray(){

    const xmlhttp = new XMLHttpRequest();
    
    // Este es como un callback después de recibir datos,
    // es decir, que vamos hacer con los datos.
    xmlhttp.onload = function(){

        // Obtener o capturar el dato de respuesta
        // usando la propiedad o atributo responseText
        const data = this.responseText;
        
        // Convertirlo a un objeto JSON
        const obj_json = JSON.parse(data);

        // 
        const response = document.querySelector("#response");
        response.innerHTML = `Array : ${ data } <br> JSON : ${ obj_json } `;

    }

    // Establecer el método y la URL de la petición o request
    xmlhttp.open("GET", "acciones.php");

    // Enviar la petición o request
    xmlhttp.send();

}