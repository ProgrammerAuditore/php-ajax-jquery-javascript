const $btnCargar = document.querySelector("#btn-cargar");
$btnCargar.onclick = function (){ fncCargarArchivo(); }

function fncCargarArchivo(){

    const xmlhttp = new XMLHttpRequest();

    // Este es como un callback al recebir un dato
    // es decir, que vamos hacer con el dato recibido
    xmlhttp.onreadystatechange = function(){
        
        // * Comprobar el estado de la petición o request
        //  status : 200 ;; Todo ha salido bien con el servidor
        // readyState : 4 ;; Se ha enviado y recibido un respuesta
        if( this.readyState == 4 && this.status == 200 ){
            
            // Obtener o capturar los datos recibidos
            const data = this.responseText;

            // Agregar la respues en un div html
            const response = document.querySelector("#response");
            response.innerHTML = data;

        }

    }

    // Especifica la petición o request 
    // método GET , un archivo llamadao cargar.txt
    xmlhttp.open("GET", "cargar.txt");

    // Envía la petición o request al servidor
    // Usado para solicitudes GET
    xmlhttp.send();

}