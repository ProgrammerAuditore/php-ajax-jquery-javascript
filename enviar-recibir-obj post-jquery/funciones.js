window.onload =  function() {  request();  };

// Jquery implementado a JavaScript
// $("#btn-enviar").click(function(){  request();  });

function request(){

    // Crear un objeto JavaScript
    const datos =  {
        nombre: "",
        edad: 0,
        estadoCivil: "",
        telefono: 0,
    }

    // Convetirlo en un cadena objeto JSON
    const cadena_json = JSON.stringify( datos );

    // Enviar una peticioón (request) al servidor
    // usando Ajax o JQuery
    $.post("acciones.php", cadena_json,
    function(data, response){

        // Convertirlo a objeto JavaScript
        const obj_js = JSON.parse(data);
        
        $("#nombre").val( obj_js.nombre );
        $("#edad").val( obj_js.edad );
        $("#estado-civil").val( obj_js.estadoCivil );
        $("#telefono").val( obj_js.telefono );
        

    });
    
}