<?php

    // * Comprobar si hay una petición o request de tipo POST
    if($_POST){

        // Obtener el body de la petición y convertirlo a un objeto PHP
        $obj = json_decode(file_get_contents("php://input"));

        // Definir los datos al objeto
        $obj->nombre = "Marcos";
        $obj->edad = "23";
        $obj->estadoCivil = "Anonimo";
        $obj->telefono = "0123456789";

        // Enviar un cadena de objeto JSON
        print json_encode($obj);
        
    }

?>