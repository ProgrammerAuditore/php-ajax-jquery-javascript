const $btnEnviar = document.querySelector("#btn-enviar");

$btnEnviar.onclick = function(){ fncEnviarRecibirDatos(); }

// async ; Indica que vamos a usar una promise
async function fncEnviarRecibirDatos(){
    
    const miPromesa = new Promise(function(resolve, reject){
        
        let xmlHttp = new XMLHttpRequest();
        
        // Establecer el metodo de envio
        xmlHttp.open("POST", "acciones.php", false);

        // Este funciona como un callback, es decir,
        // que hacer despues de recibir los datos
        xmlHttp.onload = function(){

            if(this.readyState == 4 && this.status == 200){
                resolve( this.responseText );
                
            }else{
                reject("Error");
                
            }

        }

        // Se define que la peticion es enforma de tupla,
        // es decir, que tiene llave => valor 
        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
        // Se envia la petición de tipo POST
        xmlHttp.send("name=Ford&edad=24");

    });
    
    // * Establecemos la respuesta
    // await ; Indica que vamos a esperar la respuesta de la promise
    document.querySelector("#response").innerHTML = await miPromesa;

}