window.onload = function () {
    fncCargarArchivo();    
}

function fncCargarArchivo(){
    
    const xmlhttp = new XMLHttpRequest();
    
    // Este funciona como un callback,
    // es decir, que hacer con el dato recibido
    xmlhttp.onload = function () {
        fncObtenerNodos(this);
    }

    // Establece el tipo de petición o request
    // metodo GEST, archivo .xml 
    xmlhttp.open("GET", "cargar.xml");

    // Enviar la petición o request GET 
    xmlhttp.send();

}

function fncObtenerNodos(e) { 
    
    // Convetir XML a objeto JavaScript 
    const xmlDoc = e.responseXML;

    // Obtener todo los nodos de CANCION
    const nodos = xmlDoc.getElementsByTagName("CANCION");
    let tr = td = "";

    for(let i=0; i < nodos.length; i++){
        tr += "<tr>";
        td += `<td><img src="${ nodos[i].getElementsByTagName("PORTADA")[0].childNodes[0].nodeValue }"/></td>`;
        td += `<td>${ nodos[i].getElementsByTagName("ARTISTA")[0].childNodes[0].nodeValue }</td>`;
        td += `<td>${ nodos[i].getElementsByTagName("TITULO")[0].childNodes[0].nodeValue }</td>`;
        td += `<td>${ nodos[i].getElementsByTagName("GENERO")[0].childNodes[0].nodeValue }</td>`;
        td += `<td>${ nodos[i].getElementsByTagName("ALBUM")[0].childNodes[0].nodeValue }</td>`;
        tr += td + "</tr>";
        td = "";
    }

    // Obtener todos los nodos CANCION
    const trCanciones = document.querySelector("#tr-canciones");
    trCanciones.innerHTML = tr;

}